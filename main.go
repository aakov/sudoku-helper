package main

import (
	"fmt"
	"os"

	"gitlab.com/aakov/sudoku-helper/cmd"

	"github.com/urfave/cli"
	"gitlab.com/aakov/sudoku-helper/version"
)

func main() {
	app := cli.NewApp()
	app.Usage = "Sudoku Helper Tool"
	app.Version = fmt.Sprintf("%s (revision %s)", version.Version, version.Revision)

	app.Commands = []cli.Command{
		cmd.NewKIllerCommand(),
	}

	app.Run(os.Args)
}

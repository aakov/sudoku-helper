package version

var (
	// Version represents the version of the Migrations CLI.
	Version = "1.0.0"

	// Revision is the GIT revision of the Migrations CLI.
	Revision = ""
)


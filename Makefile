.DEFAULT_GOAL := build

GO111MODULE ?= on
BINARY_NAME ?= sudoku-cli
GIT_REVISOIN := $(shell git rev-list -1 HEAD)

build:
	go build -v \
	-o "bin/${BINARY_NAME}" \
	-ldflags "-X gitlab.com/aakov/sudoku-helper/version.Revision=${GIT_REVISOIN}"

dep:
	go mod tidy -v
	go mod vendor -v
	go mod verify

install: build
	install -m 0755 bin/${BINARY_NAME} ${GOPATH}/bin/${BINARY_NAME}

uninstall:
	rm -rf ${GOPATH}/bin/${BINARY_NAME}

test:
	go test -v -cover -mod=vendor ./...

clean:
	go clean -v
	rm -f bin/${BINARY_NAME}

format:
	go fmt .

.PHONY: build install uninstall clean format darwin linux windows release

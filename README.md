# Sudoku Helper
This repository contains the Sudoku Helper source code.

## Requirements

* [Go](https://golang.org/) v1.13.0 at least.

## Installation

In order to install `sudoku-cli`, follow these steps after
cloning the repository.

```sh
make install
```

The `sudoku-cli` binary will be installed in your `$GOPATH/bin` directory.

## Examples
The following commands are examples of how you can use the cli.

List available commands and options. You can drill down for each command:
```sh
sudoku-cli --help
sudoku-cli killer --help
sudoku-cli killer sum --help
```
#### Killer sum
Cage size and total must be specified, always included/excluded numbers are optional.
Command and output:

```sh
sudoku-cli killer sum -s 4 -t 26
[[2 7 8 9] [3 6 8 9] [4 5 8 9] [4 6 7 9] [5 6 7 8]]
Numbers always out: [1]
```
With excluded provided:

```sh
sudoku-cli killer sum -s 4 -t 26 -e 32 
[[4 5 8 9] [4 6 7 9] [5 6 7 8]]
Numbers always out: [1 2 3]
```
You can of course also use just included.

With both included and excluded:

```sh
sudoku-cli killer sum -s 4 -t 26 -e 32 -i 9
[[4 5 8 9] [4 6 7 9]]
Numbers always in: [4 9]
Numbers always out: [1 3 2]

sudoku-cli killer sum -s 4 -t 26 -e 32 -i 89
[[4 5 8 9]]
```
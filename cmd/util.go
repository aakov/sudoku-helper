package cmd

import (
	"fmt"
	"strconv"
	"strings"
)

func numberStringToIntSlice(numbers string) (res []int, err error) {
	excludedS := strings.Split(numbers, "")
	for _, s := range excludedS {
		n, err := strconv.Atoi(s)
		if err != nil {
			err = fmt.Errorf("excluded numbers (%s) are not numbers: %s", numbers, err)
			return nil, err
		}
		res = append(res, n)
	}
	return
}

func sliceContains(slice []int, number int) bool {
	for _, n := range slice {
		if n == number {
			return true
		}
	}
	return false
}

func sliceSum(s []int) (sum int) {
	for _, n := range s {
		sum += n
	}
	return
}

func copySlice(s []int) []int {
	res := make([]int, 0)
	for _, v := range s {
		res = append(res, v)
	}
	return res
}

func getAlwaysIn(winners [][]int) (inners []int) {
	numberMap := make(map[int]int)
	for _, set := range winners {
		for _, number := range set {
			numberMap[number]++
		}
	}
	for k, v := range numberMap {
		// if in every combination
		if v == len(winners) {
			inners = append(inners, k)
		}
	}
	return
}

func getAlwaysOut(winners [][]int) (neverInners []int) {
	numberMap := make(map[int]int)
	// set all numbers to 0 so they show up in the end
	for i := 1; i <= 9; i++ {
		numberMap[i] = 0
	}

	for _, set := range winners {
		for _, number := range set {
			numberMap[number]++
		}
	}
	for k, v := range numberMap {
		// if in every combination
		if v == 0 {
			neverInners = append(neverInners, k)
		}
	}
	return
}

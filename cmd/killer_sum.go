package cmd

import (
	"fmt"

	"github.com/urfave/cli"
)

// NewSumListCommand returns a command listing possible sums according parameters
func NewKillerSumCommand() cli.Command {
	return cli.Command{
		Name:   "sum",
		Usage:  "list possible sums",
		Action: execKillerSumCmd,
		Flags: []cli.Flag{
			cli.IntFlag{
				Name:  "size, s",
				Usage: "size of the killer sudoku cage",
			},
			cli.IntFlag{
				Name:  "total, t",
				Usage: "total of the killer sudoku cage",
			},
			cli.StringFlag{
				Name:  "excluded, e",
				Usage: "excluded numbers, must all be out",
			},
			cli.StringFlag{
				Name:  "included, i",
				Usage: "included numbers, must all be in",
			},
		},
	}
}

func execKillerSumCmd(c *cli.Context) error {
	cmdName := c.Command.HelpName
	size := c.Int("size")
	if size == 0 {
		err := fmt.Errorf("No cage size specified. See '%s --help'.", cmdName)
		return cli.NewExitError(err, 64)
	}

	if size < 2 || size > 9 {
		err := fmt.Errorf("Cage size must be between 2 and 9 included.", cmdName)
		return cli.NewExitError(err, 64)
	}

	total := c.Int("total")
	if total == 0 {
		err := fmt.Errorf("No cage total specified. See '%s --help'.", cmdName)
		return cli.NewExitError(err, 64)
	}

	if total < 3 || total > 45 {
		err := fmt.Errorf("Cage total must be between 3 and 45 included.", cmdName)
		return cli.NewExitError(err, 64)
	}

	excludedString := c.String("excluded")

	excluded, err := numberStringToIntSlice(excludedString)
	if err != nil {
		return err
	}

	includedString := c.String("included")

	included, err := numberStringToIntSlice(includedString)
	if err != nil {
		return err
	}

	winners := make([][]int, 0)

	for i := 1; i <= 9; i++ {
		if sliceContains(excluded, i) {
			continue
		}
		if 9-i+2 <= size {
			break
		}
		currentSeries := []int{i}
		testCombination(total, size, currentSeries, excluded, &winners)
	}

	finalWinners := make([][]int, 0)

	// filter using included
	if len(included) > 0 {
		for _, set := range winners {
			containsAll := true
			for _, includedNumber := range included {
				if !sliceContains(set, includedNumber) {
					containsAll = false
					break
				}
			}

			if containsAll {
				// add winning set if it contains desired numbers
				finalWinners = append(finalWinners, set)
			}
		}
	} else {
		finalWinners = winners
	}

	fmt.Println(finalWinners)

	if len(finalWinners) > 1 {
		alwaysIn := getAlwaysIn(finalWinners)

		if len(alwaysIn) > 0 {
			fmt.Printf("Numbers always in: %v\n", alwaysIn)
		}

		alwaysOut := getAlwaysOut(finalWinners)

		if len(alwaysOut) > 0 {
			fmt.Printf("Numbers always out: %v\n", alwaysOut)
		}
	}

	return nil
}

func testCombination(total, size int, currentSeries, excluded []int, winners *[][]int) {
	// if current iteration excedes size/sum, return
	if len(currentSeries) > size || sliceSum(currentSeries) > total {
		return
	}

	// winner
	if len(currentSeries) == size && sliceSum(currentSeries) == total {
		// do this to avoid some weird golang bullshit causing
		// the last winner to change
		newWinner := copySlice(currentSeries)
		*winners = append(*winners, newWinner)
		return
	}

	currentSize := len(currentSeries)
	//start from the next number directly
	for i := currentSeries[currentSize-1] + 1; i <= 9; i++ {
		if sliceContains(excluded, i) {
			continue
		}
		// this line is somehow changing the newSeries from
		// the last loop iteration instead of making a new one??
		newSeries := append(currentSeries, i)
		testCombination(total, size, newSeries, excluded, winners)
	}
}

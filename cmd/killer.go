package cmd

import "github.com/urfave/cli"

// NewKIllerCommand represents an option command
// used for selecting killer sudoku subcommands.
func NewKIllerCommand() cli.Command {
	return cli.Command{
		Name:  "killer",
		Usage: "killer sudoku related commands",
		Subcommands: []cli.Command{
			NewKillerSumCommand(),
		},
	}
}
